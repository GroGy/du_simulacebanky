﻿using System;
using System.Collections.Generic;
using System.Threading;
using SimulaceBanky.app.ucty;

namespace SimulaceBanky.app
{
    public class Bank
    {
        private const int UCET_COUNT = 12;

        private const int TIME_MULTIPLIER = 10;
        private const int SIMULATION_DAY_LENGHT = 3*31;

        // Parametr v konstruktoru predem alokuje kapacitu aby se nemusela presouvat data v pameti
        private List<Ucet> m_pUcty = new List<Ucet>(UCET_COUNT);

        public Bank()
        {
            //////////  TEST  //////////

            Console.WriteLine("Creating bank accounts");
            for (int i = 0; i < 5; i++)
            {
                m_pUcty.Add(new DepozitniUcet());
            }

            for (int i = 0; i < 5; i++)
            {
                m_pUcty.Add(new UverovyUcet());
            }

            for (int i = 0; i < 2; i++)
            {
                m_pUcty.Add(new StudentskyUcet());
            }

            Console.WriteLine("Created bank accounts");


            var r = new Random(22256);

            Console.WriteLine("Starting simulation for {0} days", SIMULATION_DAY_LENGHT);

            var now = DateTime.Now;
            
            for (int i = 0; i < SIMULATION_DAY_LENGHT; i++)
            {
                var simulationDay = now + new TimeSpan(i,0,0,0);
                foreach (var ucet in m_pUcty)
                {
                    if (i % 30 == 0)
                    {
                        ucet.Vklad(r.Next(25000, 30000));
                        Console.WriteLine("{0} : Vyplaty poslany na ucet.",simulationDay);

                        (ucet as UverovyUcet)?.MesicniUpdate();
                        Console.WriteLine("{0} : Provedeny mesicni zmeny pro uverovy ucet.",simulationDay);
                    }

                    var amount = r.Next(50, 2000);
                    
                    if (!ucet.Vyber(amount))
                    {
                        if (ucet is StudentskyUcet)
                        {
                            Console.WriteLine("{0} : Pokus o vybrani ze studentskeho byl zamitnut. Částka: {1}",simulationDay,amount);
                        }
                        else
                        {
                            Console.WriteLine("{0} : Pokus o vybrani byl zamitnut. Částka: {1}",simulationDay,amount);
                        }
                    }
                    else
                    {
                        Console.WriteLine("{0} : Vybrano {1}.",simulationDay,amount);
                    }
                }

                Thread.Sleep(1000 / TIME_MULTIPLIER);
            }

            Console.WriteLine("Finished Simulation");

            Console.WriteLine("Vypsani uctu: ");
            Console.WriteLine("==========================");
            
            foreach (var ucet in m_pUcty)
            {
                Console.WriteLine(ucet.ToString());
            }
            
            Console.WriteLine("==========================");
            
            /////// END OF TEST ///////

            Console.Read();
        }
    }
}