﻿using System.Diagnostics;

namespace SimulaceBanky.app.ucty
{
    public abstract class Ucet
    {
        protected decimal m_pMoney { get; set; } = 0;
        protected uint m_pID;
        
        private static uint m_pNextID = 0;

        public Ucet()
        {
            m_pID = m_pNextID++;
        }

        public decimal GetMoneyAmount()
        {
            return m_pMoney;
        }

        public abstract bool Vklad(decimal value);
        public abstract bool Vyber(decimal value);

        public override string ToString()
        {
            return $"{nameof(m_pID)}: {m_pID}, {nameof(m_pMoney)}: {m_pMoney}";
        }

        #region GENERATED

        //GENERATED COMPARE CODE
        protected bool Equals(Ucet other)
        {
            return m_pID == other.m_pID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Ucet) obj);
        }

        public override int GetHashCode()
        {
            return (int) m_pID;
        }
        
        //END OF GENERATED COMPARE CODE
        
        #endregion
    }
}