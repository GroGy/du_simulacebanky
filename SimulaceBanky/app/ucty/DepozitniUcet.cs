﻿namespace SimulaceBanky.app.ucty
{
    public class DepozitniUcet : Ucet
    {
        public override bool Vklad(decimal value)
        {
            m_pMoney += value;
            return true;
        }

        public override bool Vyber(decimal value)
        {
            if (value > m_pMoney)
            {
                m_pMoney -= value;
                return true; 
            }
            return false;
        }

        public override string ToString()
        {
            return $"DEPOZITNI, {base.ToString()}";
        }

        public DepozitniUcet()
        {
        }
    }
}