﻿using System.Configuration;

namespace SimulaceBanky.app.ucty
{
    public class UverovyUcet : Ucet
    {
        private decimal m_pMaxPujceno { get; set; } = 3000;
        private float m_pUrok { get; set; } = 0.03f; // 1.0f == 100%
        
        public override bool Vklad(decimal value)
        {
            m_pMoney += value;
            return true;
        }

        public override bool Vyber(decimal value)
        {
            if (m_pMoney - value < m_pMaxPujceno - m_pMaxPujceno) return false;
            m_pMoney -= value;
            return true;
        }

        public void MesicniUpdate()
        {
            m_pMoney *= (decimal)(1.0f + m_pUrok);
        }

        public override string ToString()
        {
            return $"UVEROVY, {base.ToString()}, {nameof(m_pMaxPujceno)}: {m_pMaxPujceno}, {nameof(m_pUrok)}: {m_pUrok}";
        }

        public UverovyUcet()
        {
        }
    }
}