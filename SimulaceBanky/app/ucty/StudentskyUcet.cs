﻿namespace SimulaceBanky.app.ucty
{
    public class StudentskyUcet : DepozitniUcet
    {
        private decimal m_pMaxVyber { get; set; } = 500;
        public override bool Vyber(decimal value)
        {
            if (value > m_pMaxVyber)
            {
                return false;
            }
            return base.Vyber(value);
        }

        public override string ToString()
        {
            return $"STUDENTSKY ,{base.ToString()}, {nameof(m_pMaxVyber)}: {m_pMaxVyber}";
        }

        public StudentskyUcet()
        {
        }
    }
}